all: compile

bison.tab.c bison.tab.h: bison.y
	bison -t -v -d bison.y -Wcounterexamples

lex.yy.c: flex.l bison.tab.h
	flex -d flex.l

compile: nodes.c lex.yy.c bison.tab.c bison.tab.h
	gcc -g -I. -o main nodes.c lex.yy.c bison.tab.c bison.tab.h -lfl

clean:
	rm main bison.tab.c lex.yy.c bison.tab.h bison.output

test: test.c nodes.c nodes.h
	gcc -o test test.c nodes.c

.PHONY: rebuild

rebuild: | clean all