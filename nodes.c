#include <malloc.h>
#include <string.h>
#include "nodes.h"

static VariableList *variables_start = NULL;
static VariableList *variables_last = NULL;

struct VariableList {
    VariableList *next;
    Node *value;
};


Node *node_create(NodeType type) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->type = type;
    return node;
}

void node_free(Node *node) {
    if (node == NULL) return;

    if (node->left != NULL)
        node_free(node->left);

    if (node->right != NULL)
        node_free(node->right);
    free(node);
}

void print_tabs(FILE *file, int count) {
    for (int i = 0; i < count; i++) {
        fprintf(file, "  ");
    }
}

void _fprint_node(FILE *file, Node *node, int level) {
    fprintf(file, "type: %s", node_types[node->type]);
    if (node->value) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "value: %s", node->value);
    }
    if (node->left) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "left:\n");
        print_tabs(file, level + 1);
        _fprint_node(file, node->left, level + 1);
    }
    if (node->right) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "right:\n");
        print_tabs(file, level + 1);
        _fprint_node(file, node->right, level + 1);
    }
}

void fprint_node(FILE *file, Node *node) {
    _fprint_node(file, node, 0);
    fprintf(file, "\n");
}

void print_node(Node *node) {
    fprint_node(stdout, node);
}
