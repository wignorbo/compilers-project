from typing import Optional

from yaml import load, CLoader

from pprint import pprint
from dataclasses import dataclass

with open("ast.yml") as ast:
    data = load(ast, Loader=CLoader)


@dataclass
class Node:
    type: str
    value: Optional[str]
    left: Optional['Node']
    right: Optional['Node']


def convert_dict_to_node(data: dict):
    if not data:
        return None

    return Node(
        type=data['type'],
        value=data.get('value'),
        left=convert_dict_to_node(data.get('left')),
        right=convert_dict_to_node(data.get('right'))
    )


def convert_program(root: Node) -> str:
    cycle_counter = 0
    lines = ["jal x1, MAIN"]

    var_list_node: Node = root.left
    vars = {}
    while var_list_node:
        vars[var_list_node.left.value] = len(vars)
        var_list_node = var_list_node.right
    lines.extend(f"{var}: data 0 * 1" for var in vars)

    lines.append("MAIN:")

    def parse_expression(expr: Node) -> list:
        lines = []
        operand = expr.left
        const = operand.left
        if const.value:
            lines.append(f"addi r1, r31, {const.value}")
        else:
            lines.append(f"addi r1, r2, 0")
        return lines

    def parse_statement(stmt: Node) -> list:
        nonlocal cycle_counter
        lines = []
        stmt = stmt.left
        match stmt.type:
            case "NODE_ST_ASSIGN":
                lines.extend(parse_expression(stmt.left))
                lines.append(f"sw rs30, {vars[stmt.value]}, rs1")
            case "NODE_ST_FOR":
                from_ = stmt.left.left.left.left.value
                to_ = stmt.left.right.left.left.value
                lines.append(f"addi r2, r31, {from_}")
                lines.append(f"addi r3, r31, {to_}")
                lines.append(f"START_CYCLE_{cycle_counter}:")
                lines.append(f"bge r2, r3, END_CYCLE_{cycle_counter}")
                lines.append(f"addi r2, r1, 1")

                body = stmt.right
                pprint(body.left)
                lines.extend(parse_statement(body))

                lines.append(f"jal r30, START_CYCLE_{cycle_counter}")
                lines.append(f"END_CYCLE_{cycle_counter}:")
                lines.append("ebreak")

            case "NODE_STR_IF":
                ...
            case "NODE_OP_THEN_ELSE":
                ...
        return lines

    st_list_node: Node = root.right
    while st_list_node:
        lines.extend(parse_statement(st_list_node.left))
        st_list_node = st_list_node.right

    return "\n".join(lines)


root = convert_dict_to_node(data)
pprint(root)
print(convert_program(root))
