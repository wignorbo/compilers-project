%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern int yylineno;
extern int yylex();
extern int yyparse();
extern FILE* yyin;

void yyerror(const char *s);
%}

%code requires { #include "nodes.h" }
%union {
    char *string;
    Node *node;
}

%type <node> Program Variable_Declaration Variable_List Variable Computation_Description Statement_List
%type <node> Statement Assignment Expression Subexpression Start_Value End_Value If_Statement Loop_List Loop_Statement
%type <node> Complex_Statement Operand
%type <string> Unary_Op Binary_Op
%type <string> EQUALS MINUS PLUS MULTIPLY DIVIDE LESS_THAN GREATER_THAN EQUAL_TO IDENTIFIER CONSTANT

%token VAR SEMICOLON COMMA EQUALS MINUS PLUS MULTIPLY DIVIDE LESS_THAN GREATER_THAN EQUAL_TO
%token FOR ASSIGN DO IF THEN ELSE LEFT_BRACKET RIGHT_BRACKET LEFT_PAREN RIGHT_PAREN TO
%token IDENTIFIER CONSTANT


%%
Program
: Variable_Declaration Computation_Description {
    $$ = node_create(NODE_PROGRAM);
    $$->left = $1;
    $$->right = $2;

    FILE *fp = fopen("ast.yml", "w");
    fprint_node(fp, $$);
    fclose(fp);

    printf("Result has been printed to ast.yml\n");

    node_free($$);
}
;

Variable_Declaration
: VAR Variable_List SEMICOLON {$$ = $2;}
;

Variable_List
: Variable {$$ = node_create(NODE_VAR_LIST); $$->left = $1;}
| Variable COMMA Variable_List {$$ = node_create(NODE_VAR_LIST); $$->left = $1; $$->right = $3;}
;

Variable : IDENTIFIER {$$ = node_create(NODE_VAR); $$->value = $1;}

Computation_Description
: LEFT_BRACKET Statement_List RIGHT_BRACKET { $$ = $2;}
;

Statement_List
: Statement {$$ = node_create(NODE_STATEMENT_LIST); $$->left = $1;}
| Statement Statement_List {$$ = node_create(NODE_STATEMENT_LIST); $$->left = $1; $$->right = $2;}
;

Statement
: Assignment {$$ = node_create(NODE_STATEMENT); $$->left = $1;}
| Complex_Statement {$$ = node_create(NODE_STATEMENT); $$->left = $1;}
;

Assignment
: IDENTIFIER EQUALS Expression SEMICOLON {$$ = node_create(NODE_ST_ASSIGN); $$->value = $1; $$->left = $3;}
;

Expression
: Unary_Op Subexpression {$$ = node_create(NODE_EXPRESSION); $$->value = $1; $$->left = $2;}
| Subexpression { $$ = node_create(NODE_EXPRESSION); $$->left = $1; }
;

Subexpression
: LEFT_PAREN Expression RIGHT_PAREN {
    $$ = node_create(NODE_SUBEXPRESSION);
    $$->left = $2;
}
| Operand {
    $$ = node_create(NODE_OPERAND);
    $$->left = $1;
}
| Subexpression Binary_Op Subexpression {
    $$ = node_create(NODE_SUBEXPRESSION);
    $$->value = $2;
    $$->left = $1;
    $$->right = $3;
}
;

Unary_Op
: MINUS
;

Binary_Op
: MINUS
| PLUS
| MULTIPLY
| DIVIDE
| LESS_THAN
| GREATER_THAN
| EQUAL_TO
;

Operand
: IDENTIFIER {$$ = node_create(NODE_OP_IDENT); $$->value = $1;}
| CONSTANT {$$ = node_create(NODE_OP_CONST); $$->value = $1;}
;

Complex_Statement
: Loop_Statement
| If_Statement
;

Loop_Statement
: FOR IDENTIFIER ASSIGN Loop_List DO Statement {
    $$ = node_create(NODE_ST_FOR);
    $$->value = $2;
    $$->left = $4;
    $$->right = $6;
}
;

Loop_List
: Start_Value TO End_Value {
    $$ = node_create(NODE_RANGE);
    $$->left = $1;
    $$->right = $3;
}
;

If_Statement
: IF Expression THEN Statement ELSE Statement { $$ = node_create(NODE_ST_IF); $$->left = $2; $$->right = node_create(NODE_OP_THEN_ELSE); $$->right->left=$4; $$->right->right=$6; }
| IF Expression THEN Statement { $$ = node_create(NODE_ST_IF); $$->left = $2; $$->right = $4;}
;

Start_Value
: Expression
;

End_Value
: Expression
;

%%

int main(int argc, char **argv) {
    yyin = stdin;
    yyparse();

    return 0;
}

void yyerror(const char *str) {
    fprintf(stderr, str);
}

