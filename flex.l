%{
#include <string.h>
#include "nodes.h"
#include "bison.tab.h"
%}

%%

"Var"            { return VAR; }
";"              { return SEMICOLON; }
","              { return COMMA; }
"="              { return EQUALS; }
"-"              { return MINUS; }
"+"              { return PLUS; }
"*"              { return MULTIPLY; }
"/"              { return DIVIDE; }
"<"              { return LESS_THAN; }
">"              { return GREATER_THAN; }
"=="             { return EQUAL_TO; }
"FOR"            { return FOR; }
":="             { return ASSIGN; }
"DO"             { return DO; }
"IF"             { return IF; }
"THEN"           { return THEN; }
"ELSE"           { return ELSE; }
"["              { return LEFT_BRACKET; }
"]"              { return RIGHT_BRACKET; }
"("              { return LEFT_PAREN; }
")"              { return RIGHT_PAREN; }
"TO"             { return TO; }

[a-z]+           { yylval.string = strdup(yytext); return IDENTIFIER; }
[0-9]+           { yylval.string = strdup(yytext); return CONSTANT; }


[ \t\n]          // Skip whitespace, tabs, and newlines

"{.+}"           // Skip comments

.                { /* Unknown character */ }

%%

int yywrap() {
    return 1;
}

