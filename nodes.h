#ifndef COMPILERS_NODES_H
#define COMPILERS_NODES_H

#include <stdio.h>

typedef enum {
    NODE_PROGRAM,

    NODE_VAR_LIST,
    NODE_VAR,

    NODE_STATEMENT_LIST,
    NODE_STATEMENT,

    NODE_UNOP_NEG,

    NODE_OPERAND,
    NODE_OP_IDENT,
    NODE_OP_CONST,

    NODE_ST_ASSIGN,
    NODE_ST_FOR,
    NODE_ST_IF,
    NODE_OP_THEN_ELSE,

    NODE_SUBEXPRESSION,
    NODE_EXPRESSION,

    NODE_RANGE
} NodeType;

static const char *node_types[] = {
        "NODE_PROGRAM",

        "NODE_VAR_LIST",
        "NODE_VAR",

        "NODE_STATEMENT_LIST",
        "NODE_STATEMENT",

        "NODE_UNOP_NEG",

        "NODE_OPERAND",
        "NODE_OP_IDENT",
        "NODE_OP_CONST",

        "NODE_ST_ASSIGN",
        "NODE_ST_FOR",
        "NODE_ST_IF",
        "NODE_OP_THEN_ELSE",

        "NODE_SUBEXPRESSION",
        "NODE_EXPRESSION",

        "NODE_RANGE"
};

typedef struct Node {
    NodeType type;
    char *value;
    struct Node *left;
    struct Node *right;
} Node;

Node *node_create(NodeType type);
void node_free(Node *node);

void fprint_node(FILE *file, Node *node);
void print_node(Node *node);

struct VariableList;
typedef struct VariableList VariableList;

Node *node_create(NodeType type);



#endif //COMPILERS_NODES_H
