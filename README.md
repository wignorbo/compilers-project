# Разработка компиляторов. Проект

Вариант: 3

---

Во всех вариантах все переменные должны быть объявлены до начала вычислений.

- <Буква> – буква латинского алфавита (a...z).
- <Цифра> – цифра от 0 до 9.

- Предусмотреть вывод значений переменных.

---

## Комментарии

Комментарий в стиле Паскаля

```
{   Первая строка комментария.
    Вторая строка комментария.
    Последняя строка комментария.	}
```

## Грамматика

```
<Программа> ::= <Объявление переменных> <Описание вычислений>
<Описание вычислений> ::= [ <Список операторов> ]
<Объявление переменных> ::= Var <Список переменных> ;
<Список переменных> ::= <Идент> | <Идент> , <Список переменных>
<Список операторов> ::= <Оператор> | <Оператор> <Список операторов>
<Оператор>::=<Присваивание> |<Сложный оператор> 
<Присваивание> ::= <Идент> = <Выражение> ;
<Выражение> ::= <Ун.оп.> <Подвыражение> | <Подвыражение>
<Подвыражение> :: = ( <Выражение> ) | <Операнд> |
< Подвыражение > <Бин.оп.> <Подвыражение>
<Ун.оп.> ::= "-"
<Бин.оп.> ::= "-" | "+" | "*" | "/"|"<"|">"|"=="
<Операнд> ::= <Идент> | <Const>
|<Сложный оператор>:: =<Оператор цикла> | <Оператор IF>
<Оператор цикла>:: =FOR <Идент> ":=" <СписокЦикла> DO <Оператор>
<СписокЦикла> :: = <НачЗнач> TO <КонЗнач>
<Оператор IF> ::= IF <Выражение> THEN <Оператор> |
IF <Выражение> THEN <Оператор> ELSE <Оператор>  
<НачЗнач> :: =  <Выражение> 
<КонЗнач>:: =  <Выражение>
<Идент> ::= <Буква> <Идент> | <Буква>
<Const> ::= <Цифра> <Const> | <Цифра>
```

## Пример программы

```
{   Пример программы на 
      выдуманном языке    } 

Var x, y, result;
[
    result = 0;
    x = 5;
    FOR y := 1 TO 10 * x DO
        IF y < 25
        THEN result = result + y;
        ELSE result = result - y ;
    
    IF result < 0
    THEN result = -result;
]
```

## AST-дерево в формате YAML



## Транслированная программма

